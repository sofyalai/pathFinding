#include <stdio.h>
#include <algorithm>
#include <vector>
#include <set>
#include <list>
#include <limits.h>
#include <mutex>
#include <map>
#include <thread>

using namespace std;

const int NON_EXIST = -1;
bool debug = false;
mutex findPathMutex;

struct Coordinate
{
   int x;
   int y;
};

/*! A class to ceate a directed acyclic graph and find the shortest path.
 */
class Graph
{
public:
   /*!@brief Constructor of the graph
    * @param nMapWidth   Width of the map
    * @param nMapHeight  Height of the map
    * @param pMap Pointer to the map definition.
    */
   Graph(const int nMapWidth, const int nMapHeight, const unsigned char *pMap);

   /*!@brief Get the shotest path distance and the route
    * @param startCoord   Coordinate of the start point
    * @param targetCoord  Coordinate of the target point
    * @param route        Route of the shortest path
    * @return Distance of the shortest path
    */
   int getShortestPath(Coordinate startCoord, Coordinate targetCoord, list<int>& route);

private:
   int m_nMapWidth;     /*!< Width of the map */
   int m_nMapHeight;    /*!< Height of the map */
   vector<int> m_validNodes;

   /*! @brief A struct to store adjacent nodes information of a node
    */
   struct Edge
   {
      int vertex;
      int weight;
   };
   map<int, list<Edge> > m_edges;

   /*!@brief Add edge
    * @param vertex0   One of the vertex of the edge
    * @param vertex1   The other vertex of the edge
    * @param weight    Weight of the edge
    */
   void addEdge(int vertex0, int vertex1, int weight);

   /*!@brief Set the directed acyclic graph from the map definition.
    * @param pMap Pointer to the map definition.
    */
   void setDAGraph(const unsigned char *pMap);

   /*!@brief Get the coordinate's node (aka the index in the map(pMap))
    * @param coord   Coordinate
    * @return Index of the coordinates in the map
    */
   inline int getNode(const Coordinate& coord) const
   {
         return coord.x + coord.y * m_nMapWidth;
   };
};

Graph::Graph(const int nMapWidth, const int nMapHeight, const unsigned char *pMap)
   : m_nMapWidth(nMapWidth), m_nMapHeight(nMapHeight)
{
   const int EDGE_MAX = 4;
   for (int i = 0; i< m_nMapWidth * m_nMapHeight; ++i)
   {
      if (pMap[i] > 0)
      {
         m_validNodes.push_back(i);
         m_edges[i].get_allocator().allocate(EDGE_MAX);
      }
   }

   setDAGraph(pMap);
}

void Graph::addEdge(int vertex0, int vertex1, int weight)
{
   m_edges[vertex0].push_back(Edge{vertex1, weight});
   m_edges[vertex1].push_back(Edge{vertex0, weight});
}

void Graph::setDAGraph(const unsigned char *pMap)
{
   for (int i = 0; i < m_nMapWidth * m_nMapHeight; ++i)
   {
      if (pMap[i] == 0)  // skip the impassable location.
      {
         continue;
      }

      int x = i % m_nMapWidth;
      int y = i / m_nMapWidth;

      if (x+1 < m_nMapWidth)
      {
         int xPos = getNode(Coordinate{x+1, y});
         if (pMap[xPos] != 0)
         {
            addEdge(i, xPos, pMap[xPos]);
            if (debug)
            {
               printf("addEdge(%d, %d, %d)\n", i, xPos, pMap[xPos]);
            }
         }
      }

      if (y+1 < m_nMapHeight)
      {
         int yPos = getNode(Coordinate{x, y+1});

         if (pMap[yPos] != 0)
         {
            addEdge(i, yPos, pMap[yPos]);
            if (debug)
            {
               printf("addEdge(%d, %d, %d)\n", i, yPos, pMap[yPos]);
            }
         }
      }
   }
}

int Graph::getShortestPath(Coordinate startCoord, Coordinate targetCoord, list<int>& route)
{
   set< pair<int, int> > visited;
   map<int, pair<int, int>> dist; // pair<distance, previousNode>

   for (vector<int>::const_iterator it = m_validNodes.begin();
        it != m_validNodes.end(); ++it)
   {
      dist[*it] = make_pair(INT_MAX, NON_EXIST);
   }

   int startN =  getNode(startCoord);
   int targetN = getNode(targetCoord);

   dist[startN] = make_pair(0, NON_EXIST);  // set distance of start node to 0
   visited.insert(make_pair(dist[startN].first, startN));    // add the startNode to the visited sets.

   while(!visited.empty())
   {
      int i = (*(visited.begin())).second;   // the node to visit
      visited.erase(visited.begin());

      if (debug)
      {
         printf("dist[%d]: %d\n", i, dist[i].first);
      }

      for (list<Edge>::const_iterator it = m_edges[i].begin();
           it!= m_edges[i].end(); ++it)
      {
         int v = (*it).vertex;
         if (debug)
         {
            printf("m_edges[%d] = {%d, %d}\n", i, v, (*it).weight);
         }

         if (v == NON_EXIST)
         {
            continue;  // skip the non exist location.
         }
         int w = (*it).weight;

         if (dist[v].first > (dist[i].first + w))
         {
            if (dist[v].first != INT_MAX)
            {
               visited.erase(visited.find(make_pair(dist[v].first, v)));
            }

            dist[v].first = dist[i].first + w;
            dist[v].second = i;

            if (debug)
            {
               printf("update to dist[%d]= %d, preNode = %d \n", v, dist[v].first, dist[v].second);
            }

            if (v == targetN)
            {
               int n  = targetN;
               while(n != 0)
               {
                  route.push_front(n);  // store the route
                  n = dist[n].second;
               }
               return dist[v].first;
            }
            visited.insert(make_pair(dist[v].first, v));
         }
      }
   }

   return NON_EXIST;
}

int FindPath(const int nStartX, const int nStartY,
   const int nTargetX, const int nTargetY,
   const unsigned char* pMap,
   const int nMapWidth, const int nMapHeight,
   int* pOutBuffer, const int nOutBufferSize)
{
   lock_guard<std::mutex> lock(findPathMutex);
   if (nStartX >= nMapWidth || nStartY >= nMapHeight)
   {
      if (debug)
      {
         printf("Error: Start position is out of range.\n");
      }
      return NON_EXIST;
   }

   if (nTargetX >= nMapWidth || nTargetY >= nMapHeight)
   {
      if (debug)
      {
         printf("Error: Target position is out of range.\n");
      }
      return NON_EXIST;
   }

   Graph graph(nMapWidth, nMapHeight, pMap);

   list<int> route;
   int distance = graph.getShortestPath(Coordinate{nStartX, nStartY},
      Coordinate{nTargetX, nTargetY}, route);

   // give up if nOutBufferSize is too small.
   if (route.size() > static_cast<unsigned int>(nOutBufferSize))
   {
      if (debug)
      {
         printf("Error: The buffer size is too small.\n");
      }
      return NON_EXIST;
   }

   copy(route.begin(),route.end(), pOutBuffer);

   // print the result
   if (debug)
   {
      printf("Shortest Path from (%d, %d) to (%d, %d):\n",
         nStartX, nStartY, nTargetX, nTargetY);
      printf("Distance: %d", distance);

      if (distance > 0)
      {
         printf("\nPath:");
         for (int i = 0; i < nOutBufferSize; ++i)
         {
            printf(" %d", pOutBuffer[i]);
         }
      }
      printf("\n");
   }

   return distance;
};

int main(void)
{

   unsigned char pMap0[] =
      {1, 1, 1, 1,
       0, 1, 0, 1,
       0, 1, 1, 1,
       1, 1, 1, 1,
       0, 1, 0, 1,
       0, 1, 1, 1,
       1, 1, 1, 1,
       0, 1, 0, 1,
       0, 1, 1, 1,
       1, 1, 1, 1,
       0, 1, 0, 1,
       0, 1, 1, 1
   };
     int pOutBuffer0[12];
     thread th1(FindPath, 0, 0, 1, 2, pMap0, 4, 12, pOutBuffer0, 12);

     unsigned char pMap1[] = {0, 0, 1, 0, 1, 1, 1, 0, 1};
     int pOutBuffer1[7];
     thread th2(FindPath, 2, 0, 0, 2, pMap1, 3, 3, pOutBuffer1, 7);

     int pOutBuffer2[12];
     thread th3(FindPath, 0, 1, 1, 2, pMap0, 4, 3, pOutBuffer2, 12);

     int pOutBuffer3[12];
     thread th4(FindPath, 0, 0, 3, 2, pMap0, 4, 3, pOutBuffer3, 12);

     int pOutBuffer4[7];
     thread th5(FindPath, 2, 0, 1, 2, pMap1, 3, 3, pOutBuffer4, 7);

     int pOutBuffer5[12];
     thread th6(FindPath, 0, 0, 3, 2, pMap0, 4, 3, pOutBuffer5, 4);

     th1.join();
     th2.join();
     th3.join();
     th4.join();
     th5.join();
     th6.join();

   return 0;
}
